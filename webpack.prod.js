const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin"); // chuyển css thành file
const bootstrapEntryPoints = require('./webpack.bootstrap.config'); // config bootstrap 
const glob = require('glob');
const path = require('path');
const PurifyCSSPlugin = require('purifycss-webpack'); // giảm dung lượng css 

const extractSass = new ExtractTextPlugin({
    filename: "css/[name].css",
});
module.exports = merge(common, {
    // entry: {
    //     boostrap: bootstrapEntryPoints.prod,
    // },
    module: {
        loaders: [
            {
                test: /\.scss|css$/,
                use: extractSass.extract({
                    publicPath: '../',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                minimize: true,
                                sourceMap: true,
                                ignoreOrder: true,
                                allChunks: true
                            }
                        }, 
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: true,
                                ignoreOrder: true,
                                allChunks: true
                            }
                        },
                    ],
                    fallback: "style-loader",
                })
            },
            { test: /\.(woff2?|svg)$/, loader: 'url-loader?limit=10000&name=[name].[ext]&outputPath=fonts/&publicPath=../fonts/' },
            { test: /\.(ttf|eot)$/, loader: 'file-loader?name=[name].[ext]&outputPath=fonts/&publicPath=../fonts/' },
        ],
    },
    plugins: [
        extractSass,
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new PurifyCSSPlugin({
            // Give paths to parse for rules. These should be absolute!
            paths: glob.sync(path.join(__dirname, 'src/*.pug')),
            purifyOptions: {
                minify: true,
                whitelist: ['*']
            }
        })
      ]
});