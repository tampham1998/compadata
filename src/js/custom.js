function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
$(document).ready(function() {
    $("#login_form").validate({
        rules: {
            email: {
                required: true
            },
            pass: {
                required: true
            },
        },
        messages: {
            email: {
                required: 'Vui lòng nhập số email'
            },
            pass: {
                required: "Vui lòng nhập mật khẩu",
            },
        }
    });
});