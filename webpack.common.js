const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const webpack = require("webpack");
const AddAssetHtmlPlugin = require("add-asset-html-webpack-plugin");

module.exports = {
	entry: {
		app: "./src/app.js"
	},
	output: {
		filename: "js/[name].bundle.js",
		path: path.resolve(__dirname, "dist")
	},
	module: {
		loaders: [
			{
				test: /\.pug$/,
				use: ["pug-loader?pretty"]
			},
			{
				test: /\.jpg|png|gif|jpeg$/,
				use: [
					"file-loader?name=[name].[ext]&outputPath=images/", // export file vao folder nay
					"image-webpack-loader" // minify image
				]
			}
		]
	},
	plugins: [
		new webpack.ProvidePlugin({
			jQuery: "jquery",
			"window.jQuery": "jquery",
			$: "jquery"
		}),
		new CleanWebpackPlugin(["dist"]), // xóa folder dist
		new webpack.NamedModulesPlugin(), // dùng cho cái dưới
		new webpack.HotModuleReplacementPlugin(), // auto reload page khi css thay đổi
		new HtmlWebpackPlugin({
			title: "Home",
			filename: "index.html",
			template: "./src/index.pug"
		}),
		new HtmlWebpackPlugin({
			title: "Biểu phí",
			filename: "bieu-phi.html",
			template: "./src/bieu-phi.pug"
		}),
		new HtmlWebpackPlugin({
			title: "Liên hệ ",
			filename: "lien-he.html",
			template: "./src/lien-he.pug"
		}),
		new AddAssetHtmlPlugin([
			{
				filepath: require.resolve("./src/js/jquery.validate.min", "js"),
				publicPath: "js/",
				outputPath: "js",
				includeSourcemap: false // add this parameter
			},
			{
				filepath: require.resolve("./src/js/custom", "js"),
				publicPath: "js/",
				outputPath: "js",
				includeSourcemap: false // add this parameter
			}
		])
	],
	resolve: {
		extensions: [".js", ".jsx"],
		alias: {
			TweenLite: path.resolve(__dirname, "src/js/TweenLite")
		}
	}
};
