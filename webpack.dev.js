const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const bootstrapEntryPoints = require('./webpack.bootstrap.config')
const Dotenv = require('dotenv-webpack');

module.exports = merge(common, {
    // entry: {
    //     boostrap: bootstrapEntryPoints.dev,
    // },
    devServer: {
        contentBase: './dist',
        compress: true,
        hot: true,
        open: true,
        inline: true,
    },
    plugins: [
        new Dotenv()
    ],
    module: {
        loaders: [
            {
                test: /\.scss|css$/,
                use: ['style-loader', 'css-loader', 'sass-loader'] // cai nay no moi load live
            },
            { test: /\.(woff2?|svg)$/, loader: 'url-loader?limit=10000&name=fonts/[name].[ext]' },
            { test: /\.(ttf|eot)$/, loader: 'file-loader?name=fonts/[name].[ext]' },
        ],
    },
});